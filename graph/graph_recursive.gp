#!/usr/bin/gnuplot

set title "Rekursiv"
set datafile separator ";"
set xlabel "N"
set ylabel "T(N)"
set terminal png size 800,550
set style line 1 lw 2 ps 2 lc rgb 'red'

set style line 100 lt 1 lw 1
set grid xtics ytics mxtics mytics ls 100 lc rgb "black", ls 100 lc rgb "gray"
set logscale xy

set output "aufwand_rekursiv.png"
plot "PascalRekursiv.csv" wi li ls 1 notitle
