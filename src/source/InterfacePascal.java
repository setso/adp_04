package source;

interface InterfacePascal {

	long[] getRow(int N);
	long getCounter();
}
