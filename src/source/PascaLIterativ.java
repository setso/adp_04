package source;

public class PascaLIterativ implements InterfacePascal {
	
	private long counter;

	@Override
	public long[] getRow(int N) {
		int row = N+1;
        if( N == 0)
        {
            long res[] = new long[1];
            res[0] = 1;
            return res;
        }
                
        long result[] = new long[row];
        long cache[][] = new long[row][];

        for(int i = 0; i < row; i++)
        {
            cache[i] = new long[i+1];
            cache[i][0] = 1; //erste spalte
            cache[i][i] = 1; //diagonalelement
            for(int j = 1; j < i; j++) //werte im inneren berechenen
            {
                counter++;
                cache[i][j] = cache[i-1][j] + cache[i-1][j-1];
            }
        }
        
        for(int i = 0;i < result.length; i++)
        {
            counter++;
            result[i] = cache[row-1][i];
        }     
                
        return result;
	}

	@Override
	public long getCounter() {
		long count = counter;
		counter = 0;
		return count;
		
	}


}
