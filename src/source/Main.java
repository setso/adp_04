package source;

public class Main {

	public static void main(String[] args) 
	{
		testPascal(new PascalBinomial());
		testPascal(new PascalRekursiv());
		testPascal(new PascaLIterativ());
	}
	
	public static void testPascal(InterfacePascal pascal) 
	{
		long[] pascalArray;
		
		System.out.println("Now runnig " + pascal.getClass().getSimpleName());
		for (int N = 0; N < 100; N+=1)
		{
			pascalArray = pascal.getRow(N);
			System.out.print(N);
			System.out.print(";" + pascal.getCounter());
			
			/*
			for (int i = 0; i <= N; i++)
			{
				System.out.print( pascalArray[i] + "\t");
			}*/
			System.out.println();
		}
	}

}
