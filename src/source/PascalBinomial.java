package source;

import java.math.BigInteger;

public class PascalBinomial implements InterfacePascal {

	long _counter = 0;
	BigInteger _fakulArray[];
	
	@Override
	public long[] getRow(int N) 
	{
		_fakulArray = new BigInteger[N+1];
		long[] result = new long[N+1];
		
		//Vereinfachte Formel
		for (int k = 0; k <= N; k++)
		{
			
			// N! / (k! * (N-k)!)
			result[k] = fakultaet(N).divide((fakultaet(k).multiply(fakultaet(N - k)))).longValue();
			_counter++;
		}
		
		
		return result;
	}

	@Override
	public long getCounter() 
	{
		long count = _counter;
		_counter = 0;
		return count;
	}
	
	private BigInteger fakultaet(int N)
	{
		BigInteger result = BigInteger.ONE;
		
		if (_fakulArray[N] != null)
			if (_fakulArray[N].equals(BigInteger.ZERO))
				return _fakulArray[N];
			
		for (long i = 2; i <= N; i++)
		{
			result = result.multiply(BigInteger.valueOf(i));
			_counter++;
		}

		_fakulArray[N] = result;
		
		return result;
	}

}
