package source;

public class PascalRekursiv implements InterfacePascal {
   private long _counter = 0;

   @Override
   public long[] getRow(int N) {
      long[] row = new long[N+1];
      row[0] = 1;
      if (N > 0) {
         long[] prevRow = getRow(N-1);
         row[N] = 1;
         for (int i = 1; i < row.length - 1; i++) {
            row[i] = prevRow[i - 1] + prevRow[i];
            _counter++;
         }
      }
      return row;
   }
   
   @Override
   public long getCounter() {
      long count = _counter;
	   _counter = 0;
      return count;
   }

}
